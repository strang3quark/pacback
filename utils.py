"""
 pacback - (c) Bruno Jesus 2018
 Licensed under GPLv3
"""

import pacman
import os

def save_state(directory, filename):
    #gets the list of installed packages and saves it on @destination
    
    if (is_dir_writable(directory)):
        return list_to_file(os.path.join(directory,filename), installed_packages())
    else:
        raise Exception("Cannot write in: {0}".format(directory))

def installed_packages():
    return [p['id'] for p in pacman.get_installed()]

def refresh_repo():
    return pacman.refresh_verbose()

def install_packages(packages):
    return pacman.install_verbose(packages)

def remove_packages(packages):
    return pacman.remove_verbose(packages)

def is_dir_writable(directory):
    return os.access(directory, os.W_OK)

def get_dir_and_filename(path):
    if path != None and len(path)>1:
        #if ends with / it is a directory
        directory, filename = os.path.split(path)
        return [directory, filename]
    
    return [os.getenv("HOME"),'']

def file_exists(path):
    return os.path.isfile(path)

def dir_exists(path):
    return os.path.isdir(path)

def list_to_file(path, list_to_save):
    f = open(path, 'w')
    for line in list_to_save:
        f.write(line + '\n')
    f.close()

def file_to_list(path):
    f = open(path, 'r')
    return f.read().splitlines()

def list_differences(list1, list2):
    return [list(set(list1) - set(list2)), list(set(list2)-set(list1))]

