# PacBack
Utility to save the list of installed packages and roll back

This utility simply makes a list of installed packages and lets you revert to these packages later.

**PacBack is not well tested, use it at your own risk**

## Compatibility
I only tested it on ArchLinux, it will probably work just fine on Arch based distros like Manjaro.
You will need need Python3 installed to run this software.
This tool cannot install AUR packages, I'll probably change it to use Packer when installed.

I've taken the Pacman interface from [this repo](https://github.com/peakwinter/python-pacman).


## Usage

### Save package list
    pacback export <path_to_file_to_save>(optional)
  
### Show differences
    pacback diff path_to_file_to_load
  
### Revert to snapshot

#### Install missing packages
    pacback revert-removed path_to_file_to_load

#### Remove installed packages
    packback revert-installed path_to_file_to_load
  
#### Both (Remove installed and Install missing)
    packback revert path_to_file_to_load
    
    

## Install
Just clone the repo or download the files and you are ready to go.
You can save on a directory of your choice and then symlink or create an alias to packback.py
