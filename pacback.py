#!/usr/bin/env python
"""
 pacback - (c) Bruno Jesus 2018
 Licensed under GPLv3
"""

import sys
import utils
import datetime

def main():
    number_args = len(sys.argv)
    
    if (number_args > 1):
        action = sys.argv[1]
    
        # EXPORT THE CURRENT INSTALLED PACKAGES TO A FILE
        if (action == 'export'):
            # the file path is the second arg
            if (number_args > 2):
                path = utils.get_dir_and_filename(sys.argv[2])
            else:
                #this will return the home folder with an empty filename
                path = utils.get_dir_and_filename(None)
    
            # if the filename is empty use the default (pacback_YYYYMMDDHHMM)
            if (path != None and path[1] == ''):
                path[1]='pacback_{0}'.format(datetime.datetime.now().strftime("%Y%m%d%H%M"))
            utils.save_state(path[0],path[1])
            return 0
    
        # REVERT OR SHOW DIFF
        elif (action == 'diff' or 'revert' in action):
            if (number_args > 2):
                path = sys.argv[2]
                if not utils.file_exists(path):
                    print("pacback: {0} is not a valid file")
                    return 0;
                else:
                    snapshot = utils.file_to_list(path)
                    installed,removed = utils.list_differences(utils.installed_packages(),snapshot)
                    
                    if action == 'diff':
                        if len(installed) == 0 and len(removed) == 0:
                            print("pacback: Nothing to do") 
                            return 0

                        if len(installed) > 0:
                            print('Installed since snapshot:')
                            installed.sort()
                            for p in installed:
                                print('\t{}'.format(p))

                        if len(removed) > 0:
                            print('\nRemoved since snapshot:')
                            removed.sort()
                            for p in removed:
                                print('\t{}'.format(p))
                    else:
                        if action == 'revert-installed' or action == 'revert':
                            if len(installed) == 0:
                                print("pacback: Nothing to remove")
                            else:
                                utils.remove_packages(installed)
                        if action == 'revert-removed' or action == 'revert':
                            if len(removed) == 0:
                                print("pacback: Nothing to install")
                            else:
                                utils.refresh_repo()
                                utils.install_packages(removed)
                    return 0

    #Nothing done, return -1
    return -1 

if main() == -1:
    print("PacBack\n")
    print("Utility to save the list of installed packages and roll back\n")
    print("Usage: ")
    print(" Save snapshot:     pacback export path_to_save_the_file(optional) ")
    print(" Show differences:  pacbak diff path_to_snapshot_file ")
    print(" Revert: ")
    print("   Install removed   packages: pacback revert-installed path_to_snapshot_file")
    print("   Remove  installed packages: pacback revert-removed path_to_snapshot_file")
    print("   Install and Remove:         pacback revert path_to_snapshot_file")
